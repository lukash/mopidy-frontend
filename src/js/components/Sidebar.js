
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router';

import Link from './Link';
import Icon from './Icon';
import Dropzones from './Fields/Dropzones';
import Thumbnail from './Thumbnail';

import * as uiActions from '../services/ui/actions';
import * as mopidyActions from '../services/mopidy/actions';

class Sidebar extends React.Component{

	constructor(props){
		super(props)
	}

	render(){
		return (
			<aside className="sidebar">
				<div className="sidebar__liner">
				<img className="logo" src={'/iris/assets/mosfet-logo.svg'} />

				<nav className="sidebar__menu">

					<section className="sidebar__menu__section">
							<Link to={"/now-playing"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="play_arrow" type="material" />
								Now playing
							</Link>
							<Link to={"/queue"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="queue_music" type="material" />
								Queue
							</Link>
							<Link to={"/search"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="search" type="material" />
								Search
							</Link>
						</section>

						<section className="sidebar__menu__section">
							<title className="sidebar__menu__section__title">Source</title>
							<Link to={"/input/aux1"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="linkedin" type="fontawesome" />
								AUX1
							</Link>
							<Link to={"/input/aux2"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="linkedin" type="fontawesome" />
								AUX2
							</Link>
							<Link to={"/input/airplay"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="apple" type="fontawesome" />
								AirPlay
							</Link>
							<Link to={"/input/internet-radio"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="rss" type="fontawesome" />
								Internet Radio
							</Link>
							<Link to={"/input/youtube"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="youtube" type="fontawesome" />
								Youtube
							</Link>

							<Link to={"/input/dlna"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="retweet" type="fontawesome" />
								DLNA
							</Link>

							<Link to={"/input/bluetooth"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="bluetooth-b" type="fontawesome" />
								Bluetooth
							</Link>

							<Link to={"/input/shared-folders"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="server" type="fontawesome" />
								Shared folders
							</Link>

							<Link to={"/input/usb"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="usb" type="fontawesome" />
								USB
							</Link>

							<Link to={"/input/spotify"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="spotify" type="fontawesome" />
								Spotify Connect
							</Link>

							<Link to={"/library/browse"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="folder" type="material" />
								Local Files
							</Link>

							<Link to={"/input/drag-n-drop"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="hand-stop-o" type="fontawesome" />
								Drag&Drop
							</Link>
						</section>

						<section className="sidebar__menu__section">
							<Link to={"/settings"} history={this.props.history} className="sidebar__menu__item" activeClassName="sidebar__menu__item--active">
								<Icon name="settings" type="material" />
								Settings
								{this.props.update_available ? <span className="status tooltip tooltip--right"><Icon name="cloud_download" className="green-text" /><span className="tooltip__content">Update available</span></span>: null}
								{!this.props.mopidy_connected || !this.props.pusher_connected ? <span className="status tooltip tooltip--right"><Icon name="warning" className="red-text" /><span className="tooltip__content">{!this.props.mopidy_connected ? <span>Mopidy not connected<br /></span> : null}{!this.props.pusher_connected ? <span>Pusher not connected<br /></span> : null}</span></span> : null}
							</Link>
						</section>

				</nav>
			    </div>

		       	<Dropzones />

		       	<div className="close" onClick={e => this.props.uiActions.toggleSidebar(false)}>
		       		<Icon name="close" />
		       	</div>

			</aside>
		);
	}
}


/**
 * Export our component
 *
 * We also integrate our global store, using connect()
 **/

const mapStateToProps = (state, ownProps) => {
	return {
		mopidy_connected: state.mopidy.connected,
		pusher_connected: state.pusher.connected,
		spotify_enabled: state.spotify.enabled,
		spotify_authorized: state.spotify.authorization,
		update_available: (state.pusher.version && state.pusher.version.update_available ? state.pusher.version.update_available : false),
		test_mode: (state.ui.test_mode ? state.ui.test_mode : false),
		dragger: state.ui.dragger
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		uiActions: bindActionCreators(uiActions, dispatch),
		mopidyActions: bindActionCreators(mopidyActions, dispatch)
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Sidebar));
