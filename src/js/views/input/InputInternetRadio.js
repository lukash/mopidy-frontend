
import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Route, Switch } from 'react-router-dom';

import Link from '../../components/Link';

import Header from '../../components/Header';
import List from '../../components/List';
import TrackList from '../../components/TrackList';
import GridItem from '../../components/GridItem';
import DropdownField from '../../components/Fields/DropdownField';
import Icon from '../../components/Icon';
import URILink from '../../components/URILink';
import ErrorBoundary from '../../components/ErrorBoundary';

import * as helpers from '../../helpers';
import * as uiActions from '../../services/ui/actions';
import * as mopidyActions from '../../services/mopidy/actions';
import * as radioActions from '../../services/radio/actions';

class InputInternetRadio extends React.Component{

	constructor(props) {
		super(props);
	}

	componentDidMount() {
		console.log("LOADING STATIONS");
		this.loadStations();
		this.props.uiActions.setWindowTitle("Internet Radio");
	}

	componentWillReceiveProps(nextProps) {
		// mopidy goes online
		if (!this.props.mopidy_connected && nextProps.mopidy_connected){
			this.loadStations(nextProps);
		}
	}

	loadStations(props = this.props) {
		if (props.mopidy_connected){
			this.props.radioActions.loadStations(null);
		}
	}

	playStation(tracks) {
		console.log("PLAY URI");
		console.log(tracks);
		this.props.mopidyActions.playURIs([tracks[0].uri], tracks[0].uri);
	}

	render() {
		let listItems = [];
		if (this.props.stations) {
			for (const station of this.props.stations) {
				listItems.push({
					name: station.name,
					uri: station.uri,
					icons: []
				});
			}
		}

		return (
			<div className="view library-local-view">
				<Header>
					<Icon name="rss" type="fontawesome" />
					Internet Radios
				</Header>
				<section className="content-wrapper">
					<div className="grid grid--tiles">
						<TrackList
							show_source_icon
							track_context="queue"
							className="queue-track-list"
							tracks={listItems}
							playTracks={tracks => this.playStation(tracks)}
						/>
					</div>
				</section>
			</div>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	return {
		load_queue: state.ui.load_queue,
		mopidy_connected: state.mopidy.connected,
		stations: state.radio.stations,
	}
};

const mapDispatchToProps = (dispatch) => {
	return {
		uiActions: bindActionCreators(uiActions, dispatch),
		mopidyActions: bindActionCreators(mopidyActions, dispatch),
		radioActions: bindActionCreators(radioActions, dispatch)
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(InputInternetRadio)