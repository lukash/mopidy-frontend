
export function loadStations(uri) {
    console.log("LOADING STATIONS");
    return {
        type: 'RADIO_STATIONS_LOAD',
        uri: uri
    }
}
