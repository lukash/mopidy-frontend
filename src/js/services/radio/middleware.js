const helpers = require('./../../helpers');
const mopidyActions = require('../mopidy/actions');

const radioMiddleware = (function(){

    return store => next => action => {
        const pusher = store.getState().pusher;

        switch (action.type) {
            case "RADIO_STATIONS_LOAD":
                store.dispatch(
                    mopidyActions.request(
                        'library.browse',
                        {
                            uri: 'radio:'
                        },
                    response => {
                        if (response.length <= 0){
                            return;
                        }
                        store.dispatch({
                            type: "RADIO_STATIONS_LOADED",
                            stations: response
                        });
                    }
                ));

                next(action);
                break;

            default:
                return next(action);
        }
    }
})();

export default radioMiddleware