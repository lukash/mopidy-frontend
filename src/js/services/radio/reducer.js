
export default function reducer(radio = {}, action){

    switch (action.type) {

        case 'RADIO_STATIONS_LOADED':
            console.log("RADIO_STATIONS_LOADED");
            return Object.assign({}, radio, {
                stations: action.stations
            });

        default:
            return radio
    }
}
