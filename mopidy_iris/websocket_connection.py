
from __future__ import unicode_literals

import logging
import sys

from tornado.escape import json_encode

if sys.platform == 'win32':
    pass

# import logger
logger = logging.getLogger(__name__)


class WebsocketConnection:

    def __init__(self):
        self.connections = {}

    ##
    # Connections
    #
    # Contains all our connections and client details. This requires updates
    # when new clients connect, and old ones disconnect. These events are broadcast
    # to all current connections
    ##

    def get_connections(self, *args, **kwargs):
        callback = kwargs.get('callback', None)

        connections = [self.connections[connectionId]['client'] for connectionId in self.connections]
        response = {
            'connections': connections
        }
        if callback:
            callback(response)
        else:
            return response

    def add_connection(self, *args, **kwargs):
        connection = kwargs.get('connection', None)
        client = kwargs.get('client', None)

        self.connections[client['connection_id']] = {
            'client': client,
            'connection_id': client['connection_id'],
            'connection': connection
        }

        self.broadcast(data={
            'method': 'connection_added',
            'params': {
                'connection': client
            }
        })

    def update_connection(self, *args, **kwargs):
        callback = kwargs.get('callback', None)
        data = kwargs.get('data', {})

        print(data)

        connection_id = data['connection_id']
        if connection_id in self.connections:
            self.connections[connection_id]['client']['username'] = data['username']
            self.connections[connection_id]['client']['client_id'] = data['client_id']
            self.broadcast(data={
                'method': "connection_changed",
                'params': {
                    'connection': self.connections[connection_id]['client']
                }
            })
            response = {
                'connection': self.connections[connection_id]['client']
            }
            if callback:
                callback(response)
            else:
                return response

        else:
            error = 'Connection "'+data['connection_id']+'" not found'
            logger.error(error)

            error = {
                'message': error
            }
            if callback:
                callback(False, error)
            else:
                return error

    def remove_connection(self, connection_id):
        if connection_id in self.connections:
            try:
                client = self.connections[connection_id]['client']
                del self.connections[connection_id]
                self.broadcast(data={
                    'method': "connection_removed",
                    'params': {
                        'connection': client
                    }
                })
            except:
                logger.error('Failed to close connection to '+ connection_id)

    def set_username(self, *args, **kwargs):
        callback = kwargs.get('callback', None)
        data = kwargs.get('data', {})
        connection_id = data['connection_id']

        if connection_id in self.connections:
            self.connections[connection_id]['client']['username'] = data['username']
            self.broadcast(data={
                'method': "connection_changed",
                'params': {
                    'connection': self.connections[connection_id]['client']
                }
            })
            response = {
                'connection_id': connection_id,
                'username': data['username']
            }
            if callback:
                callback(response)
            else:
                return response

        else:
            error = 'Connection "'+data['connection_id']+'" not found'
            logger.error(error)

            error = {
                'message': error
            }
            if callback:
                callback(False, error)
            else:
                return error

    def send_message(self, *args, **kwargs):
        callback = kwargs.get('callback', None)
        data = kwargs.get('data', None)

        logger.debug(data)

        # Catch invalid recipient
        if data['recipient'] not in self.connections:
            error = 'Connection "'+data['recipient']+'" not found'
            logger.error(error)

            error = {
                'message': error
            }
            if callback:
                callback(False, error)
            else:
                return error

        # Sending of an error
        if 'error' in data:
            message = {
                'jsonrpc': '2.0',
                'error': data['error']
            }

        # Sending of a regular message
        else:
            message = {
                'jsonrpc': '2.0',
                'method': data['method'] if 'method' in data else None
            }
            if 'id' in data:
                message['id'] = data['id']
            if 'params' in data:
                message['params'] = data['params']
            if 'result' in data:
                message['result'] = data['result']

        # Dispatch the message
        try:
            self.connections[data['recipient']]['connection'].write_message(json_encode(message))

            response = {
                'message': 'Sent message to '+data['recipient']
            }
            if callback:
                callback(response)
            else:
                return response
        except:
            error = 'Failed to send message to '+ data['recipient']
            logger.error(error)

            error = {
                'message': error
            }
            if callback:
                callback(False, error)
            else:
                return error

    def broadcast(self, *args, **kwargs):
        callback = kwargs.get('callback', None)
        data = kwargs.get('data', None)

        logger.debug(data)

        if 'error' in data:
            message = {
                'jsonrpc': '2.0',
                'error': data['error']
            }
        else:
            message = {
                'jsonrpc': '2.0',
                'method': data['method'] if 'method' in data else None,
                'params': data['params'] if 'params' in data else None
            }

        for connection in self.connections.itervalues():
            send_to_this_connection = True

            # Don't send the broadcast to the origin, naturally
            if 'connection_id' in data:
                if connection['connection_id'] == data["connection_id"]:
                    send_to_this_connection = False

            if send_to_this_connection:
                print('Iris websocket message sending: ' + json_encode(message))
                connection['connection'].write_message(json_encode(message))

        response = {
            'message': 'Broadcast to '+str(len(self.connections))+' connections'
        }
        if callback:
            callback(response)
        else:
            return response
