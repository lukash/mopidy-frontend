
from __future__ import unicode_literals

import json
import logging
import os
import random
import string
import time
import urllib2
from datetime import datetime

import tornado.ioloop
import tornado.template
import tornado.web
import tornado.websocket
from mopidy_iris.rpc.rpc import IrisRPC
from mopidy_iris.websocket_connection import WebsocketConnection
from tornado.escape import json_decode

logger = logging.getLogger(__name__)


class WebsocketHandler(tornado.websocket.WebSocketHandler):

    # initiate (not the actual object __init__, but run shortly after)
    def initialize(self, core=None, config=None):
        self.core = core
        self.config = config
        self.websocketConnection = WebsocketConnection()
        self.rpc = IrisRPC(core, config, self.websocketConnection)

    def check_origin(self, origin):
        return True

    def select_subprotocol(self, subprotocols):
        # select one of our subprotocol elements and return it. This confirms the connection has been accepted.
        protocols = self._digest_protocol(subprotocols)

        # if we've auto-generated some ids, the provided subprotocols was a string, so just return it right back
        # this allows a connection to be completed
        return subprotocols[0] if protocols['generated'] else protocols['client_id']

    def open(self):
        # Get the client's IP. If it's local, then use it's proxy origin
        ip = self.request.remote_ip
        if ip == '127.0.0.1' and hasattr(self.request.headers,'X-Forwarded-For'):
            ip = self.request.headers['X-Forwarded-For']

        # Construct our initial client object, and add to our list of connections
        client = {
            'connection_id': generate_guid(),
            'ip': ip,
            'created': datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
        }

        self.connection_id = client['connection_id']

        self.websocketConnection.add_connection(connection=self, client=client)

    def on_message(self, message):
        logger.debug("Iris websocket message received: " + message)

        message = json_decode(message)
        message_id = message['id'] if 'id' in message else None

        if 'jsonrpc' not in message:
            self._handle_result(id=message_id, error={'id': message_id, 'code': 32602, 'message': 'Invalid JSON-RPC request (missing property "jsonrpc")'})

        if 'params' in message:
            params = message['params']

            # Handle hard-coded connection_id in messages
            # Otherwise include the origin connection of this message
            if 'connection_id' not in params:
                message['params']['connection_id'] = self.connection_id
        else:
            params = {}

        # call the method, as specified in payload
        if 'method' not in message:
            self._handle_result(error={'id': message_id, 'code': 32602, 'message': 'Method key missing from request'}, id=message_id)
            return

        # make sure the method exists
        if not hasattr(self.rpc, message['method']):
            self._handle_result(error={'id': message_id, 'code': 32601, 'message': 'Method "' + message['method'] + '" does not exist'}, id=message_id)
            return

        method = getattr(self.rpc, message['method'])
        method(data=params,
               callback=lambda response, error=False: self._handle_result(id=message_id, method=message['method'], response=response, error=error))

    def on_close(self):
        self.websocketConnection.remove_connection(connection_id=self.connection_id)

    def data_received(self, chunk):
        pass

    ##
    # Handle a response from our core
    # This is just our callback from an Async request
    ##
    def _handle_result(self, *args, **kwargs):
        id = kwargs.get('id', False)
        method = kwargs.get('method', None)
        response = kwargs.get('response', None)
        error = kwargs.get('error', None)
        request_response = {
            'id': id,
            'jsonrpc': '2.0',
            'method': method
        }

        # We've been given an error
        if error:
            error['id'] = id
            request_response['error'] = error

        # We've been handed an AsyncHTTPClient callback. This is the case
        # when our request calls subsequent external requests (eg Spotify, Genius)
        elif isinstance(response, tornado.httpclient.HTTPResponse):
            request_response['result'] = response.body

        # Just a regular json object, so not an external request
        else:
            request_response['result'] = response

        # Respond to the original request
        data = request_response
        data['recipient'] = self.connection_id
        self.rpc.send_message(data=data)

    ##
    # Digest a protocol header into it's id/name parts
    #
    # @return dict
    ##
    def _digest_protocol(self, protocol):

        # if we're a string, split into list
        # this handles the different ways we get this passed (select_subprotocols gives string, headers.get gives list)
        if isinstance(protocol, basestring):

            # make sure we strip any spaces (IE gives "element,element", proper browsers give "element, element")
            protocol = [i.strip() for i in protocol.split(',')]

        # if we've been given a valid array
        try:
            client_id = protocol[0]
            connection_id = protocol[1]
            username = protocol[2]
            generated = False

        # invalid, so just create a default connection, and auto-generate an ID
        except:
            client_id = generate_guid()
            connection_id = generate_guid()
            username = 'Anonymous'
            generated = True

        # construct our protocol object, and return
        return {
            "client_id": client_id,
            "connection_id": connection_id,
            "username": username,
            "generated": generated
        }


class HttpHandler(tornado.web.RequestHandler):

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Client-Security-Token, Accept-Encoding")

    def initialize(self, core=None, config=None):
        self.core = core
        self.config = config
        self.rpc = IrisRPC(core, config)

    # Options request
    # This is a preflight request for CORS requests
    def options(self, slug=None):
        self.set_status(204)
        self.finish()

    @tornado.web.asynchronous
    def get(self, slug=None):
        id = int(time.time())

        # make sure the method exists
        if not hasattr(self.rpc, slug):
            self._handle_result(id=id, error={'code': 32601, 'message': "Method " + slug + " does not exist"})
            return

        getattr(self.rpc, slug)(request=self, callback=lambda response, error=False: self._handle_result(id=id, method=slug, response=response, error=error))

    @tornado.web.asynchronous
    def post(self, slug=None):
        id = int(time.time())

        try:
            params = json.loads(self.request.body.decode('utf-8'))
        except:
            self._handle_result(id=id, error={'code': 32700, 'message': "Missing or invalid payload"})
            return

        # make sure the method exists
        if not hasattr(self.rpc, slug):
            self._handle_result(id=id, error={'code': 32601, 'message': "Method " + slug + " does not exist"})
            return

        try:
            method = getattr(self.rpc, slug)
            method(data=params,
                   request=self.request,
                   callback=lambda response=False, error=False: self._handle_result(id=id, method=slug, response=response, error=error))
        except urllib2.HTTPError as e:
            self._handle_result(id=id, error={'code': 32601, 'message': "Invalid JSON payload"})
            return

    def data_received(self, chunk):
        pass

    ##
    # Handle a response from our core
    # This is just our callback from an Async request
    ##
    def _handle_result(self, *args, **kwargs):
        id = kwargs.get('id', None)
        method = kwargs.get('method', None)
        response = kwargs.get('response', None)
        error = kwargs.get('error', None)
        request_response = {
            'id': id,
            'jsonrpc': '2.0',
            'method': method
        }

        if error:
            request_response['error'] = error
            self.set_status(400)

        # We've been handed an AsyncHTTPClient callback. This is the case
        # when our request calls subsequent external requests (eg Spotify, Genius).
        # We don't need to wrap non-HTTPResponse responses as these are dicts
        elif isinstance(response, tornado.httpclient.HTTPResponse):

            # Digest JSON responses into JSON
            content_type = response.headers.get('Content-Type')
            if content_type.startswith('application/json') or content_type.startswith('text/json'):
                body = json.loads(response.body)

            # Non-JSON so just copy as-is
            else:
                body = response.body

            request_response['result'] = body

        # Regular ol successful response
        else:
            request_response['result'] = response

        # Write our response
        self.write(request_response)
        self.finish()


##
# Customised handler for react router URLS
#
# This routes all URLs to the same path, so that React can handle the path etc
##
class ReactRouterHandler(tornado.web.StaticFileHandler):

    def initialize(self, path):
        self.path = path
        self.dirname, self.filename = os.path.split(path)
        super(ReactRouterHandler, self).initialize(self.dirname)

    def get(self, path=None, include_body=True):
        super(ReactRouterHandler, self).get(self.path, include_body)

    def data_received(self, chunk):
        pass


##
# Generate a random string
#
# Used for connection_ids where none is provided by client
# @return string
##
def generate_guid():
    length = 12
    return ''.join(random.choice(string.lowercase) for i in range(length))


