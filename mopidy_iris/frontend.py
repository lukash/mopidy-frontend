from __future__ import unicode_literals

import logging

import pykka
from mopidy.core import CoreListener
from mopidy_iris.snapcast import IrisSnapcastThread

logger = logging.getLogger(__name__)


class IrisFrontend(pykka.ThreadingActor, CoreListener):

    def __init__(self, config, core):
        super(IrisFrontend, self).__init__()
        self.config = config
        self.core = core
        self.snapcast_daemon = None

    def on_start(self):
        logger.info('Starting Iris')
        # Start our TCP watcher with no request, so it becomes our
        # long-running socket connection
        if self.config['iris'].get('snapcast_enabled'):
            self.snapcast_daemon = IrisSnapcastThread(self.config)
            self.snapcast_daemon.start()

    def on_stop(self):
        logger.info('Stopping Iris')
        if self.snapcast_daemon:
            logger.info('Stopping Snapcast daemon')
            self.snapcast_daemon.close()
