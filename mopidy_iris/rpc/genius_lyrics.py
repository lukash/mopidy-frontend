
from __future__ import unicode_literals

import logging
import sys

import tornado.httpclient
import tornado.ioloop
import tornado.web

if sys.platform == 'win32':
    pass

# import logger
logger = logging.getLogger(__name__)


class GeniusLyrics:

    ##
    # Genius Lyrics
    ##
    def get_lyrics(self, *args, **kwargs):
        callback = kwargs.get('callback', False)
        request = kwargs.get('request', False)
        error = False
        url = ""

        try:
            path = request.get_argument('path')
            url = 'https://genius.com' + path
        except Exception, e:
            logger.error(e)
            error = {
                'message': "Path not valid",
                'description': str(e)
            }

        try:
            connection_id = request.get_argument('connection_id')

            if connection_id not in self.connections:
                error = {
                    'message': 'Unauthorized request',
                    'description': 'Connection '+connection_id+' not connected'
                }

        except Exception, e:
            logger.error(e)
            error = {
                'message': "Unauthorized request",
                'description': "connection_id missing"
            }

        if error:
            if (callback):
                callback(False, error)
                return
            else:
                return error

        http_request = tornado.httpclient.HTTPRequest(url)
        http_client = tornado.httpclient.HTTPClient()
        http_client.fetch(http_request, callback=callback)
