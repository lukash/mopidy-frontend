
from __future__ import unicode_literals

import json
import logging
import os
import pickle
import sys

import tornado.httpclient
import tornado.ioloop
import tornado.web

if sys.platform == 'win32':
    pass

# import logger
logger = logging.getLogger(__name__)


class Commands:

    def __init__(self, core, config):
        # Load our commands from file
        self.core = core
        self.config = config
        self.commands = self._load_from_file('commands')

    '''
    Commands
    These are stored locally for all users to access
    '''
    def get_commands(self, *args, **kwargs):
        callback = kwargs.get('callback', False)

        response = {
            'commands': self.commands
        }
        if (callback):
            callback(response)
        else:
            return response

    def set_commands(self, *args, **kwargs):
        callback = kwargs.get('callback', False)
        data = kwargs.get('data', {})

        # Update our temporary variable
        self.commands = data['commands']

        # Save the new commands to file storage
        self._save_to_file(self.commands, 'commands')

        self.broadcast(data={
            'method': 'commands_changed',
            'params': {
                'commands': self.commands
            }
        })

        response = {
            'message': 'Commands saved'
        }
        if (callback):
            callback(response)
        else:
            return response

    ##
    # Save dict object to disk
    #
    # @param dict Dict
    # @param name String
    # @return void
    ##
    def _save_to_file(self, dict, name):

        # Build path to our special Iris folder
        path = self.config['core'].get('cache_dir')+'/iris/'

        # Create the folder if it doesn't yet exist
        if not os.path.exists(path):
            os.makedirs(path)

        # And now open the file, and drop in our dict
        try:
            with open(path + name + '.pkl', 'wb') as f:
                pickle.dump(dict, f, pickle.HIGHEST_PROTOCOL)
        except Exception:
            return False

    ##
    # Load a dict from disk
    #
    # @param name String
    # @return Dict
    ##
    def _load_from_file(self, name):

        # Build path to our special Iris folder
        path = self.config['core'].get('cache_dir')+'/iris/'

        try:
            with open(path + name + '.pkl', 'rb') as f:
                return pickle.load(f)
        except Exception:
            return {}

    def run_command(self, *args, **kwargs):
        callback = kwargs.get('callback', False)
        data = kwargs.get('data', {})
        error = False

        if str(data['id']) not in self.commands:
            error = {
                'message': 'Command failed',
                'description': 'Could not find command by ID "'+str(data['id'])+'"'
            }
        else:
            command = self.commands[str(data['id'])]

            if "method" not in command:
                error = {
                    'message': 'Command failed',
                    'description': 'Missing required property "method"'
                }
            if "url" not in command:
                error = {
                    'message': 'Command failed',
                    'description': 'Missing required property "url"'
                }

        if error:
            if callback:
                callback(False, error)
                return
            else:
                return error

        # Construct the request
        http_client = tornado.httpclient.HTTPClient()
        if command['method'] == 'POST':
            request = tornado.httpclient.HTTPRequest(command['url'], connect_timeout=5, method='POST', body=json.dumps(command['post_data']), validate_cert=False)
        else:
            request = tornado.httpclient.HTTPRequest(command['url'], connect_timeout=5, validate_cert=False)

        # Make the request, and handle any request errors
        try:
            command_response = http_client.fetch(request)
        except Exception as e:
            error = {
                'message': 'Command failed',
                'description': str(e)
            }
            if callback:
                callback(False, error)
                return
            else:
                return error

        # Attempt to parse JSON
        try:
            command_response_body = json.loads(command_response.body)
        except:
            command_response_body = command_response.body

        # Finally, return the result
        response = {
            'message': 'Command run',
            'response': command_response_body
        }
        if callback:
            callback(response)
            return
        else:
            return response
