from mopidy_iris.rpc.commands import Commands
from mopidy_iris.rpc.genius_lyrics import GeniusLyrics
from mopidy_iris.rpc.queue_metadata import QueueMetadata
from mopidy_iris.rpc.system import System
from mopidy_iris.snapcast import SnapcastClient


# All the RPC methods called from web frontend
class IrisRPC:

    def __init__(self, core, config, websocket_connection=None):
        self.core = core
        self.config = config
        self.websocket_connection = websocket_connection
        self.snapcast_client = SnapcastClient()
        self.system = System()
        # self.spotify_radio = SpotifyRadio(core, config)
        self.queue_metadata = QueueMetadata()
        self.genius_lyrics = GeniusLyrics()
        self.commands = Commands(core, config)

    # Snapcast ----------------------------------
    def snapcast(self, *args, **kwargs):
        self.snapcast_client.snapcast_request(args, kwargs)

    # Websocket communication ----------------------
    def send_message(self, *args, **kwargs):
        return self.websocket_connection.send_message(*args, **kwargs)

    def broadcast(self, *args, **kwargs):
        return self.websocket_connection.broadcast(*args, **kwargs)

    def get_connections(self, *args, **kwargs):
        return self.websocket_connection.get_connections(*args, **kwargs)

    def update_connection(self, *args, **kwargs):
        return self.websocket_connection.update_connection(*args, **kwargs)

    def set_username(self, *args, **kwargs):
        return self.websocket_connection.set_username(*args, **kwargs)

    # Config ------------------------------------
    def get_config(self, *args, **kwargs):
        pass

    def get_version(self, *args, **kwargs):
        pass

    # System ------------------------------------
    def restart(self, *args, **kwargs):
        self.system.restart(*args, **kwargs)

    def upgrade(self, *args, **kwargs):
        self.system.upgrade(*args, **kwargs)

    def local_scan(self, *args, **kwargs):
        pass

    # Spoify radio ------------------------------
    def get_spotify_token(self, *args, **kwargs):
        pass  # Spotify not needed
        # return self.spotify_radio.get_spotify_token(*args, **kwargs)

    def refresh_spotify_token(self, *args, **kwargs):
        pass  # Spotify not needed
        # return self.spotify_radio.refresh_spotify_token(*args, **kwargs)

    def get_radio(self, *args, **kwargs):
        pass  # Spotify not needed
        # return self.spotify_radio.get_radio(*args, **kwargs)

    def change_radio(self, *args, **kwargs):
        pass  # Spotify not needed
        # return self.spotify_radio.change_radio(*args, **kwargs)

    def stop_radio(self, *args, **kwargs):
        pass  # Spotify not needed
        # return self.spotify_radio.stop_radio(*args, **kwargs)

    # Queue metadata ----------------------------
    def get_queue_metadata(self, *args, **kwargs):
        return self.queue_metadata.get_queue_metadata(*args, **kwargs)

    def add_queue_metadata(self, *args, **kwargs):
        return self.queue_metadata.add_queue_metadata(*args, **kwargs)

    def clean_queue_metadata(self, *args, **kwargs):
        return self.queue_metadata.clean_queue_metadata(*args, **kwargs)

    # Executing commands ------------------------
    def get_commands(self, *args, **kwargs):
        return self.commands.get_commands(*args, **kwargs)

    def set_commands(self, *args, **kwargs):
        return self.commands.set_commands(*args, **kwargs)

    def run_command(self, *args, **kwargs):
        return self.commands.run_command(*args, **kwargs)

    # Lyrics -----------------------------------
    def get_lyrics(self, *args, **kwargs):
        return self.genius_lyrics.get_lyrics(*args, **kwargs)
        pass

    # Test --------------------------------------
    def test(self, *args, **kwargs):
        pass




