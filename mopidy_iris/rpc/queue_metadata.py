
from __future__ import unicode_literals

import logging
import sys

if sys.platform == 'win32':
    pass

# import logger
logger = logging.getLogger(__name__)


class QueueMetadata:
    queue_metadata = {}

    ##
    # Additional queue metadata
    #
    # This maps tltracks with extra info for display in Iris, including
    # added_by and from_uri.
    ##
    def get_queue_metadata(self, *args, **kwargs):
        callback = kwargs.get('callback', False)

        response = {
            'queue_metadata': self.queue_metadata
        }
        if (callback):
            callback(response)
        else:
            return response

    def add_queue_metadata(self, *args, **kwargs):
        callback = kwargs.get('callback', False)
        data = kwargs.get('data', {})

        for tlid in data['tlids']:
            item = {
                'tlid': tlid,
                'added_from': data['added_from'] if 'added_from' in data else None,
                'added_by': data['added_by'] if 'added_by' in data else None
            }
            self.queue_metadata['tlid_'+str(tlid)] = item

        self.broadcast(data={
            'method': 'queue_metadata_changed',
            'params': {
                'queue_metadata': self.queue_metadata
            }
        })

        response = {
            'message': 'Added queue metadata'
        }
        if (callback):
            callback(response)
        else:
            return response

    def clean_queue_metadata(self, *args, **kwargs):
        callback = kwargs.get('callback', False)
        cleaned_queue_metadata = {}

        for tltrack in self.core.tracklist.get_tl_tracks().get():

            # if we have metadata for this track, push it through to cleaned dictionary
            if 'tlid_' + str(tltrack.tlid) in self.queue_metadata:
                cleaned_queue_metadata['tlid_' + str(tltrack.tlid)] = self.queue_metadata['tlid_'+str(tltrack.tlid)]

        self.queue_metadata = cleaned_queue_metadata

