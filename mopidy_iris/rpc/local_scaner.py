
from __future__ import unicode_literals

import logging
import sys

from mopidy_iris.rpc.system import IrisSystemThread

if sys.platform == 'win32':
    pass

# import logger
logger = logging.getLogger(__name__)


class LocalScanner:

    ##
    # Run a mopidy local scan
    # Essetially an alias to "mopidyctl local scan"
    ##
    def local_scan(self, *args, **kwargs):
        callback = kwargs.get('callback', False)

        # Trigger the action
        IrisSystemThread('local_scan', self.local_scan_callback).start()

        self.broadcast(data={
            'method': "local_scan_started"
        })

        response = {
            'message': "Local scan started"
        }
        if (callback):
            callback(response)
        else:
            return response

    def local_scan_callback(self, response, error):
        if error:
            self.broadcast(data={
                'method': "local_scan_error",
                'params': error
            })
        else:
            self.broadcast(data={
                'method': "local_scan_finished",
                'params': response
            })

    ##
    # Simple test method. Not for use in production for any purposes.
    ##
    def test(self, *args, **kwargs):
        callback = kwargs.get('callback', False)

        self.broadcast(data={
            'method': "test_started"
        })

        # Trigger the action
        IrisSystemThread('test', self.test_callback).start()

        response = {
            'message': "Running test... please wait"
        }
        if (callback):
            callback(response)
        else:
            return response

    def test_callback(self, response, error):
        if error:
            self.broadcast(data={
                'method': "test_error",
                'params': error
            })
        else:
            self.broadcast(data={
                'method': "test_finished",
                'params': response
            })
