
import ctypes
import logging
import os
import subprocess
import sys
from threading import Thread

# import logger
logger = logging.getLogger(__name__)


class System:

    version = '1.0.0'

    ##
    # System controls
    #
    # Facilitates upgrades and configuration fetching
    ##
    def get_config(self, *args, **kwargs):
        callback = kwargs.get('callback', False)

        response = {
            'config': {
                "is_root": is_root(),
                "country": self.config['iris']['country'],
                "locale": self.config['iris']['locale'],
                "snapcast_enabled": self.config['iris']['snapcast_enabled']
            }
        }

        if self.spotify:
            response['config'] = self.spotify.append_config(response['config'])

        if callback:
            callback(response)
        else:
            return response

    def get_version(self, *args, **kwargs):
        callback = kwargs.get('callback', False)
        response = {
            'version': {
                'current': self.version,
                'latest': self.version,
                'is_root': is_root(),
                'upgrade_available': False
            }
        }
        if callback:
            callback(response)
        else:
            return response

    ##
    # Restart Mopidy
    # This requires sudo access to system.sh
    ##
    def restart(self, *args, **kwargs):
        callback = kwargs.get('callback', False)

        # Trigger the action
        IrisSystemThread('restart', self.restart_callback).start()

        self.broadcast(data={
            'method': "restart_started"
        })

        response = {
            'message': "Restart started"
        }
        if (callback):
            callback(response)
        else:
            return response

    def restart_callback(self, response, error):
        if error:
            self.broadcast(data={
                'method': "restart_error",
                'params': error
            })
        else:
            self.broadcast(data={
                'method': "restart_finished"
            })

    ##
    # Run an upgrade of Iris
    ##
    def upgrade(self, *args, **kwargs):
        callback = kwargs.get('callback', False)

        self.broadcast(data={
            'method': "upgrade_started"
        })

        # Trigger the action
        IrisSystemThread('upgrade', self.upgrade_callback).start()

        response = {
            'message': "Upgrade started"
        }

        if (callback):
            callback(response)
        else:
            return response

    def upgrade_callback(self, response, error):
        if error:
            self.broadcast(data={
                'method': "upgrade_error",
                'params': error
            })
        else:
            self.broadcast(data={
                'method': "upgrade_finished",
                'params': response
            })
            self.restart()


class IrisSystemThread(Thread):

    def __init__(self, action, callback):
        Thread.__init__(self)
        self.action = action
        self.callback = callback
        self.path = os.path.dirname(__file__)

    ##
    # Run the defined action
    ##
    def run(self):
        logger.info("Running system action: "+self.action)

        try:
            self.can_run()
        except Exception, e:
            logger.error(e)

            error = {
                'message': "Permission denied",
                'description': str(e)
            }

            if self.callback:
                self.callback(False, error)
            return

        # Run the actual task (this is the process-blocking instruction)
        output = subprocess.check_output(["sudo "+self.path+"/system.sh "+self.action], shell=True)

        # And then, when complete, return to our callback
        if self.callback:
            response = {
                'output': output
            }
            self.callback(response, False)


    ##
    # Check if we have access to the system script (system.sh)
    #
    # @return boolean or exception
    ##
    def can_run(self, *args, **kwargs):

        # Attempt an empty call to our system file
        process = subprocess.Popen("sudo -n "+self.path+"/system.sh", stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        result, error = process.communicate()
        exitCode = process.wait()

        # Some kind of failure, so we can't run any commands this way
        if exitCode > 0:
            raise Exception("Password-less access to "+self.path+"/system.sh was refused. Check your /etc/sudoers file.")
        else:
            return True


##
# Detect if we're running as root
##
def is_root():
    if sys.platform == 'win32':
        return ctypes.windll.shell32.IsUserAnAdmin() != 0
    else:
        return os.geteuid() == 0
