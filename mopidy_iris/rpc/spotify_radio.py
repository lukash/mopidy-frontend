
from __future__ import unicode_literals

import json
import logging
import sys
import time
import urllib
import urllib2

import pykka
import tornado.httpclient
import tornado.ioloop
import tornado.web
from mopidy.core import CoreListener

if sys.platform == 'win32':
    pass

# import logger
logger = logging.getLogger(__name__)


class SpotifyRadioFrontend(pykka.ThreadingActor, CoreListener):

    def __init__(self, config, core):
        super(SpotifyRadioFrontend, self).__init__()
        self.config = config
        self.core = core
        self.spotify_radio = SpotifyRadio(core, config)

    def on_start(self):
        logger.info('Starting Spotify Radio')

    def on_stop(self):
        logger.info('Stopping Spotify Radio')

    def track_playback_ended(self, tl_track, time_position):
        self.spotify_radio.check_for_radio_update()

    def tracklist_changed(self):
        self.spotify_radio.clean_queue_metadata()


class SpotifyRadio:

    spotify_token = False

    def __init__(self, core, config):
        self.core = core
        self.config = config

    def append_config(self, current_config):
        # handle config setups where there is no username/password
        # Iris won't work properly anyway, but at least we won't get server errors

        if 'spotify' in self.config and 'username' in self.config['spotify']:
            spotify_username = self.config['spotify']['username']
        else:
            spotify_username = False

        current_config['spotify_username'] = spotify_username
        return current_config

    ##
    # Spotify Radio
    #
    # Accepts seed URIs and creates radio-like experience. When our tracklist is nearly
    # empty, we fetch more recommendations. This can result in duplicates. We keep the
    # recommendations limit low to avoid timeouts and slow UI
    ##
    def get_radio(self, *args, **kwargs):
        callback = kwargs.get('callback', False)

        response = {
            'radio': self.radio
        }
        if (callback):
            callback(response)
        else:
            return response

    def change_radio(self, *args, **kwargs):
        callback = kwargs.get('callback', False)
        data = kwargs.get('data', {})

        # We're starting a new radio (or forced restart)
        if data['reset'] or not self.radio['enabled']:
            starting = True
            self.initial_consume = self.core.tracklist.get_consume().get()
        else:
            starting = False

        # fetch more tracks from Mopidy-Spotify
        self.radio = {
            'seed_artists': data['seed_artists'],
            'seed_genres': data['seed_genres'],
            'seed_tracks': data['seed_tracks'],
            'enabled': 1,
            'results': []
        }
        uris = self.load_more_tracks()

        # make sure we got recommendations
        if uris:
            if starting:
                self.core.tracklist.clear()

            self.core.tracklist.set_consume(True)

            # We only want to play the first batch
            added = self.core.tracklist.add(uris = uris[0:3])

            if (not added.get()):
                logger.error("No recommendations added to queue")

                self.radio['enabled'] = 0
                error = {
                    'message': 'No recommendations added to queue',
                    'radio': self.radio
                }
                if (callback):
                    callback(False, error)
                else:
                    return error

            # Save results (minus first batch) for later use
            self.radio['results'] = uris[3:]

            if starting:
                self.core.playback.play()
                self.broadcast(data={
                    'method': "radio_started",
                    'params': {
                        'radio': self.radio
                    }
                })
            else:
                self.broadcast(data={
                    'method': "radio_changed",
                    'params': {
                        'radio': self.radio
                    }
                })

            self.get_radio(callback=callback)
            return

        # Failed fetching/adding tracks, so no-go
        else:
            logger.error("No recommendations returned by Spotify")
            self.radio['enabled'] = 0
            error = {
                'code': 32500,
                'message': 'Could not start radio',
                'data': {
                    'radio': self.radio
                }
            }
            if (callback):
                callback(False, error)
            else:
                return error

    def stop_radio(self, *args, **kwargs):
        callback = kwargs.get('callback', False)

        self.radio = {
            "enabled": 0,
            "seed_artists": [],
            "seed_genres": [],
            "seed_tracks": [],
            "results": []
        }

        # restore initial consume state
        self.core.tracklist.set_consume(self.initial_consume)
        self.core.playback.stop()

        self.broadcast(data={
            'method': "radio_stopped",
            'params': {
                'radio': self.radio
            }
        })

        response = {
            'message': 'Stopped radio'
        }
        if (callback):
            callback(response)
        else:
            return response

    def load_more_tracks(self, *args, **kwargs):

        try:
            self.get_spotify_token()
            spotify_token = self.spotify_token
            access_token = spotify_token['access_token']
        except:
            error = 'IrisFrontend: access_token missing or invalid'
            logger.error(error)
            return False

        try:
            url = 'https://api.spotify.com/v1/recommendations/'
            url = url+'?seed_artists='+(",".join(self.radio['seed_artists'])).replace('spotify:artist:','')
            url = url+'&seed_genres='+(",".join(self.radio['seed_genres'])).replace('spotify:genre:','')
            url = url+'&seed_tracks='+(",".join(self.radio['seed_tracks'])).replace('spotify:track:','')
            url = url+'&limit=50'

            req = urllib2.Request(url)
            req.add_header('Authorization', 'Bearer '+access_token)

            response = urllib2.urlopen(req, timeout=30).read()
            response_dict = json.loads(response)

            uris = []
            for track in response_dict['tracks']:
                uris.append( track['uri'] )

            return uris

        except:
            logger.error('IrisFrontend: Failed to fetch Spotify recommendations')
            return False

    def check_for_radio_update( self ):
        tracklistLength = self.core.tracklist.length.get()
        if (tracklistLength < 3 and self.radio['enabled'] == 1):

            # Grab our loaded tracks
            uris = self.radio['results']

            # We've run out of pre-fetched tracks, so we need to get more recommendations
            if len(uris) < 3:
                uris = self.load_more_tracks()

            # Remove the next batch, and update our results
            self.radio['results'] = uris[3:]

            # Only add the next set of uris
            uris = uris[0:3]

            self.core.tracklist.add(uris=uris)

    ##
    # Spotify authentication
    #
    # Uses the Client Credentials Flow, so is invisible to the user. We need this token for
    # any backend spotify requests (we don't tap in to Mopidy-Spotify, yet). Also used for
    # passing token to frontend for javascript requests without use of the Authorization Code Flow.
    ##

    def get_spotify_token(self, *args, **kwargs):
        callback = kwargs.get('callback', False)

        # Expired, so go get a new one
        if (not self.spotify_token or self.spotify_token['expires_at'] <= time.time()):
            self.refresh_spotify_token()

        response = {
            'spotify_token': self.spotify_token
        }

        if (callback):
            callback(response)
        else:
            return response

    def refresh_spotify_token(self, *args, **kwargs):
        callback = kwargs.get('callback', None)

        # Use client_id and client_secret from config
        # This was introduced in Mopidy-Spotify 3.1.0
        url = 'https://auth.mopidy.com/spotify/token'
        data = {
            'client_id': self.config['spotify']['client_id'],
            'client_secret': self.config['spotify']['client_secret'],
            'grant_type': 'client_credentials'
        }

        try:
            http_client = tornado.httpclient.HTTPClient()
            request = tornado.httpclient.HTTPRequest(url, method='POST', body=urllib.urlencode(data))
            response = http_client.fetch(request)

            token = json.loads(response.body)
            token['expires_at'] = time.time() + token['expires_in']
            self.spotify_token = token

            self.broadcast(data={
                'method': 'spotify_token_changed',
                'params': {
                    'spotify_token': self.spotify_token
                }
            })

            response = {
                'spotify_token': token
            }
            if (callback):
                callback(response)
            else:
                return response

        except urllib2.HTTPError as e:
            error = json.loads(e.read())
            error = {'message': 'Could not refresh token: '+error['error_description']}

            if (callback):
                callback(False, error)
            else:
                return error