
from __future__ import unicode_literals

import logging
import os

import handlers
import tornado.web
import tornado.websocket
from frontend import IrisFrontend
from handlers import WebsocketHandler, HttpHandler
from mopidy import config, ext

logger = logging.getLogger(__name__)
__version__ = '3.37.1'


##
# Core extension class
#
# Loads config and gets the party started. Initiates any additional frontends, etc.
##
class Extension(ext.Extension):

    dist_name = 'Mopidy-Iris'
    ext_name = 'iris'
    version = __version__

    def get_default_config(self):
        conf_file = os.path.join(os.path.dirname(__file__), 'ext.conf')
        return config.read(conf_file)

    def get_config_schema(self):
        schema = config.ConfigSchema(self.ext_name)
        schema['enabled'] = config.Boolean()
        schema['country'] = config.String()
        schema['locale'] = config.String()
        schema['snapcast_enabled'] = config.Boolean()
        schema['snapcast_host'] = config.String()
        schema['snapcast_port'] = config.Integer()
        return schema

    def setup(self, registry):
        registry.add('frontend', IrisFrontend)
        registry.add('http:app', {
            'name': self.ext_name,
            'factory': iris_factory
        })


def iris_factory(config, core):
    path = os.path.join(os.path.dirname(__file__), 'static')

    return [
        (r'/http/([^/]*)', handlers.HttpHandler, {
            'core': core,
            'config': config
        }),
        (r'/ws/?', handlers.WebsocketHandler, {
            'core': core,
            'config': config
        }),
        (r'/images/(.*)', tornado.web.StaticFileHandler, {
            'path': path + '/assets'  # config['local-images']['image_dir']
        }),
        (r'/assets/(.*)', tornado.web.StaticFileHandler, {
            'path': path + '/assets'
        }),
        (r'/((.*)(?:css|js|json|map)$)', tornado.web.StaticFileHandler, {
            'path': path
        }),
        (r'/(.*)', handlers.ReactRouterHandler, {
            'path': path + '/index.html'
        }),
    ]
