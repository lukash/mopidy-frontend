Development
===========

Install
-------

    npm install
    source <mopidy_virtual_env>/bin/activate

Local server
------------
Starts local webpack-dev-server with proxy to local mopidy:

    source <mopidy_virtual_env>/bin/activate
    npm run start

Open browser on *http://localhost:8080/iris/*

Deploy to mopidy
----------------

    npm run prod 
    python ./setup.py develop

You do not need to restart mopidy after rebuilding

